# EQL5 - Embedded Qt5 Lisp #

**ECL** embedded **Qt5** binding (MIT licensed), embeddable in Qt5

see [**widget** examples](https://gitlab.com/eql/EQL5/tree/master/examples), [**qml** examples](https://gitlab.com/eql/EQL5/tree/master/examples/M-modules/quick), [**widget** screenshot](https://gitlab.com/eql/EQL5/blob/master/screenshots/examples.png), [**qml** screenshot](https://gitlab.com/eql/EQL5/blob/master/screenshots/QML.png)

* interactive development (either Slime or a top-level running the Qt event loop)
* cross-platform
* no external dependencies (the binding is fully self-contained, except for Qt5 itself)
* overriding virtual Qt functions
* unicode
* translations (Qt Linguist)
* loading UI files (Qt Designer), optionally converting x.ui to x.lisp
* simple encapsulation in Lisp classes
* easily **embeddable** in existing Qt/C++ projects
* QtWebKit: JavaScript / Lisp bridge, plugin widgets
* QtQuick: QML / Lisp bridge